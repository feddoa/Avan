#ifndef CATALOG_HPP
#define CATALOG_HPP

#include <vector>
#include <memory>

#include "car.hpp"

class Catalog final {
public: 
  
  Catalog();
  Catalog(const Catalog&) = delete;
  Catalog& operator=(const Catalog&) = delete;

  void printCatalog() const;
  void printCatalogSpeed(unsigned int speed) const;
  void printCatalogPrice(unsigned int price) const;

  std::unique_ptr<Car> getCarById(size_t id);

private:

  std::vector<std::unique_ptr<Car>> m_catalog;

};

#endif
