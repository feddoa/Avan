#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "catalog.hpp"
#include "customer.hpp"

static void showHelp() {
  std::cout << "\ncommands list:\n" << "help - shows commands list\n" << "cars - shows car list\n"
            << "cars speed [speed] - shows car list where speed is over than given speed\n"
            << "cars price [price] - shows car list where price is over than given price\n"
            << "my car - shows your car\n"
            << "buy [id] - buy car with given id\n"
            << "exit - closes program\n\n";
}

static void mainLoop() {
  std::string in;
  Catalog catalog;
  Customer customer;

  while (std::getline(std::cin, in)) {

    if (std::cin.fail() && !std::cin.eof())
      throw std::ios_base::failure("Read error");

    if (in == "help")
      showHelp();
    else if (in == "cars")
      catalog.printCatalog();
    else if (in == "my car")
      customer.getCarInfo();
    else if (in == "exit")
      return;
    else {

      std::stringstream ss(in);
      std::istream_iterator<std::string> begin(ss);
      std::istream_iterator<std::string> end;
      std::vector<std::string> args(begin, end);

      if (args.size() < 2 || args.size() > 3) {
        std::cerr << "\nInvalid argument\n";
        showHelp();
      }
      else if (args.size() == 2 && args[0] == "buy")
        customer.setCar(catalog.getCarById(std::stoul(args[1])));
      else if (args.size() == 3 && args[0] == "cars" && args[1] == "speed")
        catalog.printCatalogSpeed(std::stoul(args[2]));
      else if (args.size() == 3 && args[0] == "cars" && args[1] == "price")
        catalog.printCatalogPrice(std::stoul(args[2]));
      else {
        std::cerr << "\nInvalid argument\n";
        showHelp();
      }

    }
  }
}

int main() {
  
  try {
    mainLoop();
    return 0;
  }
  catch (std::exception& ex) {
    std::cout << ex.what() << '\n';
  }

}
