#ifndef CAR_HPP
#define CAR_HPP

#include <iostream>

enum class CarColour {
  Red,
  Yellow,
  Green,
  Count
};

static std::ostream& operator<<(std::ostream& out, const CarColour& colour) {
  switch (colour) {
  case CarColour::Red: return out << "Red\n";
  case CarColour::Yellow: return out << "Yellow\n";
  case CarColour::Green: return out << "Green\n";
  }
  return out;
}

class Car final {
public:

  Car() = delete;

  Car(unsigned int max_speed, unsigned int price, CarColour colour)
    : m_max_speed(max_speed), m_price(price), m_colour(colour) {}

  void print() const {
    std::cout << "maximum speed = " << m_max_speed << ", price = " 
              << m_price << ", colour = " << m_colour;
  }

  unsigned int getMaxSpeed() const { return m_max_speed; }
  unsigned int getPrice() const { return m_price; }

private:

  unsigned int m_max_speed = 0;
  unsigned int m_price = 0;
  CarColour m_colour = CarColour::Red;

};

#endif
