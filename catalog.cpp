#include <numeric>
#include <random>
#include <type_traits>
#include <exception>

#include "catalog.hpp"

const size_t catalogSize = 10;
const unsigned int minSpeed = 160;
const unsigned int maxSpeed = 290;
const unsigned int minPrice = 20000;
const unsigned int maxPrice = 700000;
const int startColour = static_cast<std::underlying_type_t<CarColour>>(CarColour::Red);
const int endColour = static_cast<std::underlying_type_t<CarColour>>(CarColour::Count) - 1;

Catalog::Catalog() {
  m_catalog.resize(catalogSize);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<unsigned int> speed(minSpeed, maxSpeed);
  std::uniform_int_distribution<unsigned int> price(minPrice, maxPrice);
  std::uniform_int_distribution<> colour(startColour, endColour);

  for (size_t i{}; i < catalogSize; ++i) {
    m_catalog[i] = std::make_unique<Car>(speed(gen), price(gen), static_cast<CarColour>(colour(gen)));
  }
}

void Catalog::printCatalog() const {
  std::cout << '\n';
  if (m_catalog.empty()) {
    std::cout << "Sold out!!!\n\n";
    return;
  }
  
  for (size_t i{}; const auto &car : m_catalog) {
    std::cout << "id: " << i << ',';
    car->print();
    i++;
  }
  std::cout << '\n';
}

void Catalog::printCatalogSpeed(unsigned int speed) const {
  std::cout << '\n';
  if (m_catalog.empty()) {
    std::cout << "Sold out!!!\n\n";
    return;
  }

  for (size_t i{}; const auto &car : m_catalog) {
    if (car->getMaxSpeed() >= speed) {
      std::cout << "id: " << i << ',';
      car->print();
    }
    i++;
  }
  std::cout << '\n';
}

void Catalog::printCatalogPrice(unsigned int price) const {
  std::cout << '\n';
  if (m_catalog.empty()) {
    std::cout << "Sold out!!!\n\n";
    return;
  }
  for (size_t i{}; const auto &car : m_catalog) {
    if (car->getPrice() >= price) {
      std::cout << "id: " << i << ',';
      car->print();
    }
    i++;
  }
  std::cout << '\n';
}

std::unique_ptr<Car> Catalog::getCarById(size_t id) {
  auto tmp = std::move(m_catalog.at(id));
  m_catalog.erase(m_catalog.begin() + id);
  return tmp;
}