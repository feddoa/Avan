#ifndef CUSTOMER_HPP
#define CUSTOMER_HPP

#include <memory>

#include "car.hpp"

class Customer final {
public:

  Customer() = default;
  Customer(const Customer&) = delete;
  Customer& operator=(const Customer&) = delete;

  void setCar(std::unique_ptr<Car> car) {
    m_car = std::move(car);
  }

  void getCarInfo() const {
    if (m_car == nullptr) {
      std::cout << "\nNo car\n\n";
      return;
    }

    m_car->print();
  }

private:
  std::unique_ptr<Car> m_car;
};

#endif